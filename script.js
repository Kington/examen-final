const animaux = [{
    "id": 200,
    "nom": "Gaëlle",
    "type": "Poule",
    "dieteSpeciale": true,
    "photo" : "https://i.picsum.photos/id/409/200/200.jpg?hmac=AY8BYOBixnRqVEMdEhYmw49e-6qu3M3zf_xXjkAuHHc"
}, {
    "id": 400,
    "nom": "Táng",
    "type": "Serpent",
    "dieteSpeciale": false,
    "photo" : "https://i.picsum.photos/id/128/200/200.jpg?hmac=m4AGhjLVIqDjy9qPNhbZyp8Gm_K03UAgP3IZnItJMA4"
}, {
    "id": 600,
    "nom": "Réjane",
    "type": "Poule",
    "dieteSpeciale": true,
    "photo" : "https://i.picsum.photos/id/409/200/200.jpg?hmac=AY8BYOBixnRqVEMdEhYmw49e-6qu3M3zf_xXjkAuHHc"
}, {
    "id": 800,
    "nom": "Maïly",
    "type": "Poisson",
    "dieteSpeciale": false,
    "photo" : "https://i.picsum.photos/id/716/200/200.jpg?hmac=IF3XZCw6rDCs7xOWawb1RJaXLQ6ajQuqxQcbwZM1rbE"
}, {
    "id": 1000,
    "nom": "Crééz",
    "type": "Poisson",
    "dieteSpeciale": false,
    "photo" : "https://i.picsum.photos/id/716/200/200.jpg?hmac=IF3XZCw6rDCs7xOWawb1RJaXLQ6ajQuqxQcbwZM1rbE"
}, {
    "id": 1200,
    "nom": "Pélagie",
    "type": "Serpent",
    "dieteSpeciale": false,
    "photo" : "https://i.picsum.photos/id/128/200/200.jpg?hmac=m4AGhjLVIqDjy9qPNhbZyp8Gm_K03UAgP3IZnItJMA4"
}, {
    "id": 1400,
    "nom": "Dà",
    "type": "Serpent",
    "dieteSpeciale": false,
    "photo" : "https://i.picsum.photos/id/128/200/200.jpg?hmac=m4AGhjLVIqDjy9qPNhbZyp8Gm_K03UAgP3IZnItJMA4"
}, {
    "id": 1600,
    "nom": "Salomé",
    "type": "Poisson",
    "dieteSpeciale": false,
    "photo" : "https://i.picsum.photos/id/716/200/200.jpg?hmac=IF3XZCw6rDCs7xOWawb1RJaXLQ6ajQuqxQcbwZM1rbE"
}, {
    "id": 1800,
    "nom": "Zoé",
    "type": "Poisson",
    "dieteSpeciale": false,
    "photo" : "https://i.picsum.photos/id/716/200/200.jpg?hmac=IF3XZCw6rDCs7xOWawb1RJaXLQ6ajQuqxQcbwZM1rbE"
}, {
    "id": 2000,
    "nom": "Mårten",
    "type": "Poisson",
    "dieteSpeciale": false,
    "photo" : "https://i.picsum.photos/id/716/200/200.jpg?hmac=IF3XZCw6rDCs7xOWawb1RJaXLQ6ajQuqxQcbwZM1rbE"
}]

// Afficher les cartes et essaie des données //
function Anim (){
    this.poisson = '';
    this.poule = '';
    this.serpent = '';

}

const anim = new Anim();


function getAnimal(id){
    return animaux.filter((animal)=> animal.id === id)[0];
}



function resetAnim(){
    anim.poisson = '';
    anim.poule = '';
    anim.serpent = '';
}
function getTypeAnim(type) {
    switch (type) {
        case "Poisson":
            return "poisson";
        case "Poule":
            return "poule";
        case "Serpent":
            return "serpent";
        default:
            return "";
    }

}
// Tentative d'ajouter les données du formmulaire vers un Json //
$(document).ready(function (){
    $('#Ajout').click(function (){
        let x = $("form").serializeArray();
        $.each(x, function (i,field){
            for (let animal of animaux)
            $('#Adoption').append

        });
    });
});
// Afficher les cartes //
$(document).ready(function (){
    $('#Adoption');
    for (let animal of animaux){
        $('#Adoption').append(
            `<div class="card">
                <img src="${animal.photo}" class="card-img-top rounded-circle pt-2" alt="${animal.nom}">
                    <div class="card-body">
                        <h3 class="card-title">${animal.nom}</h3>
                        <p class="card-text">${animal.type}</p>
                        <button class="btn btn-outline-primary" onclick="Adopter(${animal.id})">Adopter</button>
                    </div>            
        </div>`);
    }
})




